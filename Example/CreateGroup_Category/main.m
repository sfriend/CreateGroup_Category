//
//  main.m
//  CreateGroup_Category
//
//  Created by crazytom26 on 11/16/2017.
//  Copyright (c) 2017 crazytom26. All rights reserved.
//

@import UIKit;
#import "SFAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SFAppDelegate class]));
    }
}
