# CreateGroup_Category

[![CI Status](http://img.shields.io/travis/crazytom26/CreateGroup_Category.svg?style=flat)](https://travis-ci.org/crazytom26/CreateGroup_Category)
[![Version](https://img.shields.io/cocoapods/v/CreateGroup_Category.svg?style=flat)](http://cocoapods.org/pods/CreateGroup_Category)
[![License](https://img.shields.io/cocoapods/l/CreateGroup_Category.svg?style=flat)](http://cocoapods.org/pods/CreateGroup_Category)
[![Platform](https://img.shields.io/cocoapods/p/CreateGroup_Category.svg?style=flat)](http://cocoapods.org/pods/CreateGroup_Category)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

CreateGroup_Category is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'CreateGroup_Category'
```

## Author

crazytom26, crazytom26@163.com

## License

CreateGroup_Category is available under the MIT license. See the LICENSE file for more info.
