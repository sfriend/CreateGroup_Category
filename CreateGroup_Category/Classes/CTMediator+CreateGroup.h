//
//  CTMediator+CreateGroup.h
//  CreateGroup_Category
//
//  Created by tom on 2017/11/16.
//

#import <CTMediator/CTMediator.h>

@interface CTMediator (CreateGroup)

- (UIViewController *)CTMediator_CreateGroup:(NSDictionary *)params;

@end
