//
//  CTMediator+CreateGroup.m
//  CreateGroup_Category
//
//  Created by tom on 2017/11/16.
//

#import "CTMediator+CreateGroup.h"

@implementation CTMediator (CreateGroup)

- (UIViewController *)CTMediator_CreateGroup:(NSDictionary *)params {
    
    return [self performTarget:@"CreateGroupController" action:@"CreateGroup" params:params shouldCacheTarget:YES];
}

@end
